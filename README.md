# GSUPL-ChCore文档

说明: This is the repository of ChCore labs in OS, 2022 Autumin.


## Par 01 配置Gitee仓库
---

Repository: git@gitee.com:hcp6897/chcore-lab.git

Command line instructions

You can also upload existing files from your computer using the instructions below.

* Check the existing config files.
```
git config --list --show-origin
```

* Git global setup
```
git config --global user.name "hcp6897"
git config --global user.email "hcp6897@163.com"
```

* Create a new repository
```
git clone git@gitee.com:hcp6897/chcore-lab.git
cd chcore-lab
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
```

* Push an existing folder
```
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitee.com:hcp6897/chcore-lab.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

* Push an existing Git repository
```
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitee.com:hcp6897/chcore-lab.git
git push -u origin --all
git push -u origin --tags
```

## Par 02. 实验环境配置
---
### 2.1 配置环境

1. 安装相关软件
```
sudo apt install gcc-aarch64-linux-gnu
sudo apt install make gdb-multiarch cmake ninja-build
sudo apt install qemu-system-aarch64
sudo apt install qemu-system-arm
sudo apt install qemu-efi-aarch64
sudo apt install qemu-utils
sudo apt install git
```

2. 安装 Docker
```
sudo apt install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

sudo mkdir -p /etc/apt/keyrings

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt update

sudo apt install docker-ce docker-ce-cli containerd.io docker-compose-plugin

sudo docker run hello-world
```

### 2.2 编译问题说明

查看qemu支持的类型
```
qemu-system-aarch64 -machine help
```

查看 Makefile文件中，`-machine raspi3b` 类型是否支持！

```
QEMUOPTS = -machine raspi3b -serial null -serial mon:stdio -m size=1G -kernel $(BUILD_DIR)/kernel.img -gdb tcp::1234
```

### 2.3 实验执行

The project will be built in `build` directory.

#### 1. Docker环境下编译执行

```shell
chmod a+x scripts/docker_build.sh

./scripts/docker_build.sh

./scripts/run_docker.sh
```

#### 2：Qemu环境下编译运行

```shell
make clean

chmod a+x scripts/build.sh

./scripts/build.sh

./build/simulate.sh
```

Emulate ChCore in QEMU.

**NOTE！**
type `Ctrl+a x` to quit QEMU


### 2.4 评分 Grade (忽略)
```
make grade
```

Show your grade of labs in the current branch

## Par 03. gdb调试
---
![image](doc/01.bmp)


### 3.1 安装 gef 插件
```
git clone git clone https://github.com/hugsy/gef.git ~/GDBPlugins

echo "source ~/GDBPlugins/gef.py" > ~/.gdbinit
```
执行
```
gdb -q
```
### 3.2 安装 Tmux
```
sudo apt install tmux
```
Tmux使用方法详细参考: https://cloud.tencent.com/developer/article/1526675

### 3.3 Debug with GBD
```
make qemu-gdb
```
Start a GDB server running ChCore
  
```
make gdb
```
Start a GDB (gdb-multiarch) client

**NOTE！** 
type `Ctrl+d` to quit GDB




