BUILD_DIR := ./build
ifndef QEMU
QEMU := qemu-system-aarch64
endif

LAB := 1

# try to generate a unique GDB port
GDBPORT	:= 1234
# -serial null  Redirect the virtual serial port to host character device null.
# -serial mon:stdio  creates a multiplexed stdio backend connected to the serial port and the QEMU monitor.
# -nographic 启动的虚拟机没有图形化界面.
QEMUOPTS = -machine raspi3b -serial null -serial mon:stdio -m size=1G -kernel $(BUILD_DIR)/kernel.img -gdb tcp::1234
IMAGES = $(BUILD_DIR)/kernel.img

all: build

# .gdbinit是启动gdb-multiarch的initial command file.
gdb:
	gdb-multiarch --tui -n -x .gdbinit

build: FORCE
	./scripts/docker_build.sh

qemu: $(IMAGES) 
	$(QEMU) $(QEMUOPTS)

# -nographic: 启动的虚拟机没有图形化界面.
# -S: Do not start CPU at startup（qemu会在执行第一条指令之前暂停）.
qemu-gdb: $(IMAGES)
	@echo "***"
	@echo "*** make qemu-gdb'." 1>&2
	@echo "***"
	$(QEMU) -nographic $(QEMUOPTS) -S


gdbport:
	@echo $(GDBPORT)

docker: FORCE	
	./scripts/run_docker.sh

grade: build FORCE
	@echo "make grade"
	@echo "LAB"$(LAB)": test >>>>>>>>>>>>>>>>>"
ifeq ($(LAB), 2)
	./scripts/run_mm_test.sh
endif
	./scripts/grade-lab$(LAB)

.PHONY: clean
clean:
	@rm -rf build
	@rm -rf chcore.out

.PHONY: FORCE
FORCE:
